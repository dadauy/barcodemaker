package dada.uy.tiendada;

import java.util.List;

public class TiendadaConfiguration {

    private List<KeyValue> configurations;

    public List<KeyValue> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<KeyValue> configurations) {
        this.configurations = configurations;
    }

    public String getValue(String key) {
        for (KeyValue keyValue : this.getConfigurations()) {
            if (keyValue.getKey().equals(key)) {
                return keyValue.getValue();
            }
        }
        return null;
    }

    public static class KeyValue {
        private String key;
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
