package dada.uy.tiendada;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductService {

    private Firestore db;

    public ProductService(String projectId) throws Exception {
        createFirebaseConnection(projectId);
    }

    private void createFirebaseConnection(String projectId) throws Exception {
        FirestoreOptions firestoreOptions =
                FirestoreOptions.getDefaultInstance().toBuilder()
                        .setProjectId(projectId)
                        .setCredentials(GoogleCredentials.getApplicationDefault())
                        .build();
        Firestore db = firestoreOptions.getService();
        // [END firestore_setup_client_create_with_project_id]
        // [END fs_initialize_project_id]
        this.db = db;
    }

    public List<ProductInfo> getProducts() throws Exception {
        // asynchronously query for all products
        ApiFuture<QuerySnapshot> query = db.collection("products").whereNotEqualTo("stockType", "Unidad")/*.orderBy("name")*/.get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        ArrayList<ProductInfo> products = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            String code = document.getString("barCode");
            String name = document.getString("name");
            ProductInfo info = new ProductInfo(code, name + " - " + code);
            products.add(info);
        }

        return products.stream().sorted(ProductInfo::compare).collect(Collectors.toList());
    }

}
