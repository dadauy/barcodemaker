package dada.uy.tiendada;

public class ProductInfo {
    private String code;
    private String label;

    public ProductInfo(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }

    public static int compare(ProductInfo productInfo, ProductInfo productInfo1) {
        return productInfo.getLabel().compareTo(productInfo1.getLabel());
    }
}
