package dada.uy.tiendada;

import com.fazecast.jSerialComm.SerialPort;
import com.google.gson.Gson;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.output.bitmap.BitmapEncoder;
import org.krysalis.barcode4j.output.bitmap.BitmapEncoderRegistry;
import org.krysalis.barcode4j.tools.UnitConv;

import javax.imageio.ImageIO;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.*;
import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.*;

public class TiendadaBarCodePanel extends JPanel {
    private static final String BAUD_RATE = "baudRate";
    private static final String FIREBASE_PROJECTID = "firebase.projectId";

    private Container contentPane;

    String saveLoc = null;
    Boolean isPathChosen = false;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<ProductInfo> productCodeComboBox;
    private JComboBox<String> rackComboBox;
    private javax.swing.JButton jButtonChoose;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JLabel jLabelPeso;
    private javax.swing.JLabel jLabelProductCode;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelEstante;
    private javax.swing.JLabel jLabelPrint;
    private javax.swing.JCheckBox jCheckBoxPrint;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JFileChooser myChooser;
    private javax.swing.JComboBox<String> estanteComboBox;
    private javax.swing.JTextField weightTextField;
    private javax.swing.JLabel textField2;

    private JPanel jPanelGlobal;
    private JPanel jPanelFromImage;
    int weightImagePanel = 300;
    int heightImagePanel = 190;
    private TiendadaConfiguration tiendadaConfiguration;

    // End of variables declaration//GEN-END:variables

    public TiendadaBarCodePanel(Container contentPane) {
        this.contentPane = contentPane;
        initComponents();
    }

    private void initComponents() {

        try {
            Gson gson = new Gson();

            String pathFileProperties = System.getProperty("tiendada.configuration.path");
            System.out.println("pathFileProperties = " + pathFileProperties);
            if (pathFileProperties == null) {
                pathFileProperties = "tiendadaConfiguration.json";
            }
            File file = new File(pathFileProperties);
            System.out.println("PATH = " + file.getAbsolutePath());
            String jsonConfiguration = null;
            jsonConfiguration = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            tiendadaConfiguration = gson.fromJson(jsonConfiguration, TiendadaConfiguration.class);

            if (tiendadaConfiguration.getValue(BAUD_RATE) == null) {
                TiendadaConfiguration.KeyValue keyValue = new TiendadaConfiguration.KeyValue();
                keyValue.setKey(BAUD_RATE);
                keyValue.setValue("1200");
                tiendadaConfiguration.getConfigurations().add(keyValue);
            } else {
                System.out.println("tiendadaConfiguration.getValue(BAUD_RATE) = " + tiendadaConfiguration.getValue(BAUD_RATE));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        myChooser = new javax.swing.JFileChooser();
        jLabelPeso = new javax.swing.JLabel();
        jLabelProductCode = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        weightTextField = new javax.swing.JTextField();
        productCodeComboBox = new JComboBox<>();
        rackComboBox = new JComboBox<>();
        jButtonChoose = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jLabelEstante = new javax.swing.JLabel();
        jLabelPrint = new javax.swing.JLabel();
        jCheckBoxPrint = new javax.swing.JCheckBox();
        estanteComboBox = new JComboBox<>();
        jPanel32 = new javax.swing.JPanel();
        textField2 = new javax.swing.JLabel();

        jPanelGlobal = new javax.swing.JPanel();
        jPanelFromImage = new javax.swing.JPanel();

        myChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        myChooser.setApproveButtonText("Save here");
        myChooser.setApproveButtonToolTipText("");
        myChooser.setBackground(java.awt.Color.darkGray);
        myChooser.setDialogTitle("Configuración");
        myChooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);

        jLabelPeso.setText("Peso    :");

        jLabelProductCode.setText("Producto :");

        jLabel3.setText("Bandeja :");

        jLabel4.setText("Archivo:");

        weightTextField.setFont(new java.awt.Font("Tahoma", Font.BOLD, 14)); // NOI18N

        productCodeComboBox.setFont(new java.awt.Font("Tahoma", Font.BOLD, 14)); // NOI18N

        //rackComboBox.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jButtonChoose.setBackground(new java.awt.Color(0, 0, 0));
        jButtonChoose.setFont(jButtonChoose.getFont().deriveFont(jButtonChoose.getFont().getSize()+2f));
        jButtonChoose.setForeground(new java.awt.Color(255, 255, 255));
        jButtonChoose.setText("Choose Save Path");
        jButtonChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButtonSave.setBackground(new java.awt.Color(0, 0, 0));
        jButtonSave.setFont(jButtonSave.getFont().deriveFont(jButtonSave.getFont().getSize()+2f));
        jButtonSave.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSave.setText("Generar Codigo de Barra");
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabelEstante.setText("Estante :");
        jLabelPrint.setText("Imprimir?");
        jCheckBoxPrint.setSelected(false);

        estanteComboBox.setFont(new java.awt.Font("Tahoma", Font.BOLD, 14)); // NOI18N

        jPanel32.setBackground(new java.awt.Color(0, 0, 0));

        textField2.setBackground(new java.awt.Color(0, 0, 0));
        textField2.setFont(textField2.getFont().deriveFont(textField2.getFont().getStyle() | java.awt.Font.BOLD, textField2.getFont().getSize()+6));
        textField2.setForeground(new java.awt.Color(255, 255, 255));
        textField2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textField2.setText("LAS DOñAS BARCODE");

        // Define the panel donde va a quedar el código de barra generado
        JPanel panel = jPanelFromImage;
        panel.setLayout(new FlowLayout());

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
                jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel32Layout.createSequentialGroup()
                                .addComponent(textField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(0, 0, 0))
        );
        jPanel32Layout.setVerticalGroup(
                jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(textField2, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.contentPane);
        this.contentPane.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        //.addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelPeso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelProductCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        //.addComponent(jLabelEstante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelPrint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                )
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPanelFromImage, javax.swing.GroupLayout.PREFERRED_SIZE, weightImagePanel, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(weightTextField)
                                        .addComponent(productCodeComboBox, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                        .addComponent(rackComboBox, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                        //.addComponent(estanteComboBox, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                        .addComponent(jCheckBoxPrint, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                        //.addComponent(jButtonChoose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                ).addContainerGap(120, Short.MAX_VALUE))
                        .addComponent(jPanel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                //.addComponent(jPanelFromImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(weightTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                                        .addComponent(jLabelPeso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelProductCode, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(productCodeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(rackComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8)
//                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
//                                        .addComponent(jLabelEstante, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
//                                        .addComponent(estanteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jCheckBoxPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
//                                        .addComponent(jButtonChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                                .addComponent(jButtonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jPanelFromImage, javax.swing.GroupLayout.PREFERRED_SIZE, heightImagePanel, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                        )
//            .addGroup(layout.createSequentialGroup()
//                    .addComponent(jPanelFromImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//            )
        );

        initValues();

    }

    private void initValues() {
        weightTextField.setText("1000");

        try {

            if (tiendadaConfiguration.getValue(FIREBASE_PROJECTID) == null) {
                TiendadaConfiguration.KeyValue keyValue = new TiendadaConfiguration.KeyValue();
                keyValue.setKey(FIREBASE_PROJECTID);
                keyValue.setValue("donaelisadesa");
                tiendadaConfiguration.getConfigurations().add(keyValue);
            }
            System.out.println("tiendadaConfiguration.getValue(FIREBASE_PROJECTID) = " + tiendadaConfiguration.getValue(FIREBASE_PROJECTID));
            ProductService productService = new ProductService(tiendadaConfiguration.getValue(FIREBASE_PROJECTID));//FIXME pasarle projectId
            List<ProductInfo> products = productService.getProducts();
            productCodeComboBox.setModel(new DefaultComboBoxModel<>(products.toArray(new ProductInfo[0])));

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR: NO está configurada la variable de entorno 'GOOGLE_APPLICATION_CREDENTIALS'. Se toma unos productos de ejemplo");
            java.util.List<ProductInfo> lsProds = new ArrayList<ProductInfo>();
            ProductInfo prod1 = new ProductInfo("BIF", "BIF - Bife Ancho");
            ProductInfo prod2 = new ProductInfo("ASA", "ASA - Asado");
            lsProds.add(prod1);
            lsProds.add(prod2);
            productCodeComboBox.setModel(new DefaultComboBoxModel<>(lsProds.toArray(new ProductInfo[0])));
        }


        java.util.List<String> lsRack = new ArrayList<>();
        lsRack.add("A");
        lsRack.add("B");
        lsRack.add("C");
        lsRack.add("D");
        lsRack.add("E");
        lsRack.add("F");
        rackComboBox.setModel(new DefaultComboBoxModel<>(lsRack.toArray(new String[0])));


        java.util.List<String> lsEstante = new ArrayList<>();
        lsEstante.add("A");
        lsEstante.add("B");
        lsEstante.add("C");
        estanteComboBox.setModel(new DefaultComboBoxModel<>(lsEstante.toArray(new String[0])));

        this.processWeightFromCommPort();
    }

    private void processWeightFromCommPort() {

        Thread thread = new Thread(() -> {
            try {
                boolean conected = false;
                boolean firstTime = true;
                SerialPort[] commPorts = SerialPort.getCommPorts();
                //System.out.println("commPorts.length = " + commPorts.length);
                //JOptionPane.showMessageDialog(null, "Comm Ports size = " + commPorts.length, "Comm Ports", JOptionPane.INFORMATION_MESSAGE);

                while (!conected) {
                    //Ver ejemplos en https://github.com/Fazecast/jSerialComm/wiki/Usage-Examples
                    if (commPorts.length == 0) {
                        if (firstTime) {
                            System.out.println("WARNING: no esta conectada la balanza");
                            JOptionPane.showMessageDialog(null, "Balanza no conectada", "Advertencia", JOptionPane.WARNING_MESSAGE);
                        }
                        firstTime = false;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        commPorts = SerialPort.getCommPorts();

                    } else {
                        conected = true;
                    }
                }

                SerialPort comPort = SerialPort.getCommPorts()[0];
                comPort.openPort();

                byte stxByte = (byte) 2;
                byte endByte = (byte) 13;

                //If you were already in a different mode of operation, you can enable non-blocking/polling mode by calling the following method at any point in time
                //comPort.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
                comPort.setBaudRate(Integer.parseInt(tiendadaConfiguration.getValue(BAUD_RATE)));
                try {

                    int posChain = 0;
                    boolean startChain = false;
                    ArrayList<Integer> weightArray = new ArrayList<>();
                    while (true) {
                        while (comPort.bytesAvailable() == 0)
                            Thread.sleep(20);

                        int bytesAvailables = comPort.bytesAvailable();
                        if (bytesAvailables < 0) continue;

                        byte[] readBuffer = new byte[bytesAvailables];
                        int numRead = comPort.readBytes(readBuffer, readBuffer.length);
                        //System.out.println("Read " + numRead + " bytes.");

                        for (byte b : readBuffer) {
                            if (b == stxByte) {
                                //System.out.println("######## INICIO DE CADENA #########");
                                posChain = 1;
                                startChain = true;
                                weightArray = new ArrayList<>();
                            } else if (b == endByte) {
                                //System.out.println("######## FIN DE CADENA ######### posChain = " + posChain);
                                startChain = false;
                            } else if (startChain) {
                                //System.out.println("posChain = " + posChain);
                                if (posChain >= 5 && posChain <= 10) {
                                    int numWeight = (b & 0x0f); //Nos quedamos con los 4 ultimos bits
                                    weightArray.add(numWeight);
                                }
                                if (posChain == 10) { //Weight ended
                                    final int[] mult = {1000000};//Valor inicial del multiplicador
                                    int weightFinal = weightArray.stream().mapToInt(w -> {
                                        int ret = w * mult[0];
                                        mult[0] = mult[0] / 10;
                                        return ret;
                                    }).sum();
                                    //System.out.println("weightFinal = " + weightFinal);
                                    weightTextField.setText("" + weightFinal);
                                }
                                //System.out.println("int = " + (int)b);
                            }
                            posChain ++;
                            //this.generateStringFromByte(b);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                comPort.closePort();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        thread.start();

    }

    private String generateStringFromByte(byte b1) {
        String s2 = String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0');
        //String s2 = String.format("%8s", Integer.toBinaryString((b1 & 0xFF) + 256).substring(1));
        System.out.println(s2);
        return s2;
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        myChooser.setVisible(true);
        myChooser.showOpenDialog(this);
        saveLoc = myChooser.getSelectedFile().getAbsolutePath();
        System.out.println(saveLoc);
        if (saveLoc != null) {
            isPathChosen = true;
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String weightStr = weightTextField.getText();
        ProductInfo productCodeInfo = (ProductInfo)productCodeComboBox.getSelectedItem();
        String rackStr = (String)rackComboBox.getSelectedItem();
        String estanteStr = (String)estanteComboBox.getSelectedItem();

        String weightStrWidthZero = completeWithZero(weightStr, 5);
        String productCodeStr = completeWithZero(productCodeInfo.getCode(), 3);
        rackStr = completeWithZero(rackStr, 1);
        estanteStr = completeWithZero(estanteStr, 1);

        int sizeFinal = 14;
        int sizeExtra = sizeFinal - (1 + weightStr.length() + productCodeStr.length() + rackStr.length()/* + estanteStr.length()*/);

        String extraStr = generateRandomNumber(sizeExtra);
        extraStr = completeWithZero(extraStr, sizeExtra);

        int version = 1; //La idea es definir una versión del código de barra

        String barCode = version + productCodeStr + weightStrWidthZero + rackStr /*+ estanteStr*/ + extraStr;
        String barCodeFinal = "*" + barCode + "*";

        //Se carga en el clipboard el barCode
        boolean clipboardBarCode = true;
        if (clipboardBarCode) {
            StringSelection selection = new StringSelection(barCode);
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }

        //barCodeFinal = "*123456*";
        this.generateBarCodeImage(barCodeFinal, productCodeInfo.getLabel(), weightStr, rackStr, estanteStr);

    }//GEN-LAST:event_jButton2ActionPerformed

    String generateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

    private String completeWithZero(String text, int sizeFinal) {
        while (text.length() < sizeFinal) {
            text = "0" + text;
        }
        return text;
    }

    //Ver la página: http://barcode4j.sourceforge.net/examples.html
    private void generateBarCodeImage(String barCodeStr, String productCodeLabel, String pesoStr, String rackStr, String estanteStr) {

        try {
            String[] paramArr = new String[] {productCodeLabel + " (" + pesoStr + "g)", "Bandeja: " + rackStr/* + " - Estante: " + estanteStr*/};

            //DataMatrixBean bean = new DataMatrixBean();
            Code39Bean bean = new Code39Bean();

            final int dpi = 300;
            int ctr = 0;

            //bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); //makes the narrow bar
            //bean.setWideFactor(4);
            bean.doQuietZone(false);
            //bean.setShape(SymbolShapeHint.FORCE_RECTANGLE);
            //bean.setHeight(20d);

            String barCodeName = barCodeStr.replaceAll("\\*", "") + ".jpg";

            try {
                saveLoc = File.createTempFile("temp-file", "tmp").getParent();
                //saveLoc = "/home/usuario/Documentos/Dada/LasDonas/barcode";
                //saveLoc = "C:\\develop\\DADA\\BCODE";
                System.out.println(saveLoc);

            } catch (IOException ex) {
                ex.printStackTrace();
                //Logger. getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
            }
            File outputFile = new File(saveLoc + "/" + barCodeName);
            OutputStream out = new FileOutputStream(outputFile);
            //ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {

                boolean antiAlias = false;
                int orientation = 0;
                //Set up the canvas provider for monochrome JPEG output
                //BitmapCanvasProvider canvas = new BitmapCanvasProvider( out, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, antiAlias, orientation);

                //Set up the canvas provider to create a monochrome bitmap
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(dpi, BufferedImage.TYPE_BYTE_BINARY, antiAlias, orientation);


                //Generate the barcode
                bean.generateBarcode(canvas, barCodeStr);
                //Signal end of generation
                canvas.finish();

                //Get generated bitmap
                BufferedImage symbol = canvas.getBufferedImage();


                int fontSize = 35; //pixels
                int lineHeight = (int)(fontSize * 1.2);
                Font font = new Font("Arial", Font.BOLD, fontSize);
                int width = symbol.getWidth();
                int height = symbol.getHeight();
                //System.out.println("Barcode width = " + width);
                //System.out.println("Barcode height = " + height);
                FontRenderContext frc = new FontRenderContext(new AffineTransform(), antiAlias, true);
                for (int i = 0; i < paramArr.length; i++) {
                    String line = paramArr[i];
                    Rectangle2D bounds = font.getStringBounds(line, frc);
                    width = (int)Math.ceil(Math.max(width, bounds.getWidth()));
                    height += lineHeight;
                }

                //Add padding
                int padding = 7;
                width += 2 * padding;
                height += 3 * padding;

                BufferedImage bitmap = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_BINARY);
                Graphics2D g2d = (Graphics2D)bitmap.getGraphics();
                g2d.setBackground(Color.white);
                g2d.setColor(Color.black);
                g2d.clearRect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                g2d.setFont(font);

                //Place the barcode symbol
                AffineTransform symbolPlacement = new AffineTransform();
                symbolPlacement.translate(padding, padding);
                g2d.drawRenderedImage(symbol, symbolPlacement);

                //Add text lines (or anything else you might want to add)
                int y = padding + symbol.getHeight() + padding;
                for (int i = 0; i < paramArr.length; i++) {
                    String line = paramArr[i];
                    y += lineHeight;
                    g2d.drawString(line, padding, y);
                }
                g2d.dispose();

                //Encode bitmap as file
                String mime = "image/jpeg";
                try {
                    final BitmapEncoder encoder = BitmapEncoderRegistry.getInstance(mime);
                    encoder.encode(bitmap, out, mime, dpi);
                } finally {
                    out.close();
                }

            } finally {
                out.close();
            }

            if (jPanelFromImage != null) {
                jPanelFromImage.removeAll();
                try {
                    BufferedImage wPic = ImageIO.read(outputFile);
                    ImageIcon imageIcon = new ImageIcon(wPic);
                    ImageIcon imageIcon2 = new ImageIcon(imageIcon.getImage().getScaledInstance(weightImagePanel - 10, heightImagePanel - 40, Image.SCALE_SMOOTH));
                    JLabel barCodeLabelImage = new JLabel(imageIcon2);
                    jPanelFromImage.add(barCodeLabelImage);

                    JTextField barCodeLabel = new JTextField();
                    barCodeLabel.setText(barCodeStr);

                    Font font = new Font("Arial", Font.BOLD, 20);
                    barCodeLabel.setFont(font);
                    barCodeLabel.setEditable(false);
                    jPanelFromImage.add(barCodeLabel);

                    jPanelFromImage.revalidate();
                    jPanelFromImage.repaint();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            boolean print = jCheckBoxPrint.isSelected();
            if (print) {
                PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
                pras.add(new Copies(1));
                PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.JPEG, pras);
                if (pss.length == 0)
                    throw new RuntimeException("No printer services available.");
                PrintService printService = pss[0];
                for (PrintService ps : pss) {
                    if (ps.getName().contains("233B")) {
                        printService = ps;
                        System.out.println("Printing to '" + ps + "'");
                    } else {
                        System.out.println("Printer: '" + ps + "'");
                    }
                }
                DocPrintJob job = printService.createPrintJob();

                boolean portrait = true;
                // set up the attributes
                if (portrait) {
                    pras.add(OrientationRequested.PORTRAIT);
                }
                else {
                    pras.add(OrientationRequested.LANDSCAPE);
                }

                pras.add(new MediaPrintableArea(10, 0, 45, 28, MediaSize.MM));

                FileInputStream input = new FileInputStream(outputFile.getAbsoluteFile());
                //InputStream input = new ByteArrayInputStream(out.toByteArray());

                Doc doc = new SimpleDoc(input, DocFlavor.INPUT_STREAM.JPEG, null);
                job.print(doc, pras);
                input.close();
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

}
